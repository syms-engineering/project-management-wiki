##### Project lifecycle
- There are five phases to a project lifecycle; known as process groups. Each process group represents a series of inter-related processes to manage the work through a series of distinct steps to be completed.
- Typical development phases of an engineering project
  1. Initiating
  2. Planning
  3. Executing
  4. Monitoring and Controlling
  5. Closing